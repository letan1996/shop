export default {
  lang: "Việt Nam",
  home: {
    menusNav: [
      { title: "Trang chủ" },
      {
        title: "Sản phẩm",
      },
      { title: "Bài viết" },
      { title: "Giới thiệu" },
      { title: "Liên hệ" },
    ],
  },
};
