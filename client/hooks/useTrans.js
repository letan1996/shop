import { useRouter } from 'next/router'
import data from '../public/lang';

const useTrans = () => {
  const { locale } = useRouter();
  return data[locale];
}

export default useTrans;