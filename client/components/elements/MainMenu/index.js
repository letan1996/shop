import React from "react";
import { useTrans } from "hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function MainMenu() {
  const trans = useTrans();
  const menusNav = trans?.home?.menusNav ?? [];
  return (
    <ul className="nav__list nav main-menu">
      {menusNav.map((menu, index) => {
        return (
          <li className="nav__item" key={index}>
            {menu.title}{" "}
            {menu.children && menu.children.length > 0 && (
              <FontAwesomeIcon
                className="dropdown-icon"
                icon={["fas", "chevron-down"]}
              />
            )}
            {menu.children && menu.children.length > 0 && (
              <ul className="nav-sub">
                <li className="nav-sub__item">Hoa hồng</li>
                <li className="nav-sub__item">Hoa hồng</li>
                <li className="nav-sub__item">Hoa hồng</li>
                <li className="nav-sub__item">Hoa hồng</li>
              </ul>
            )}
          </li>
        );
      })}
    </ul>
  );
}
