import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { animated, useSpring } from "react-spring";
import { closeModal } from "redux/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { renderCurrencyVN } from "utils/common";

const ModalProduct = ({ style }) => {
  const dispatch = useDispatch();
  const { modalProps: product, isVisibleModal } = useSelector(
    (state) => state.modal
  );

  const closeModalProduct = () => {
    dispatch(closeModal());
  };

  const springs = useSpring({ from: { opacity: 0 }, to: { opacity: 1 } });

  return (
    <>
      {isVisibleModal && (
        <animated.div
          style={springs}
          className="modal__over"
          onClick={closeModalProduct}
        ></animated.div>
      )}
      <div className="modal__wrapper">
        <animated.div style={style} className="modal">
          <div className="modal__content">
            <div className="modal__img">
              <img src={product?.images[0]?.url} />
            </div>
            <div className="modal__body">
              <span className="modal__text-name">{product.title}</span>
              <span className="modal__price-current">
                {renderCurrencyVN(product.priceCurrent)}
              </span>
              <span className="modal__price-old">
                {renderCurrencyVN(product.priceOld)}
              </span>
              <div className="modal__control">
                <label className="modal__input-label" for="tentacles">
                  Số lượng:
                </label>
                <input
                  className="modal__input-count"
                  type="number"
                  id="tentacles"
                  name="tentacles"
                  min="1"
                  max="100"
                ></input>
              </div>
            </div>
          </div>
          <button className="modal__button-close" onClick={closeModalProduct}>
            <FontAwesomeIcon icon={["fas", "times"]} size="lg" color="#000" />
          </button>
        </animated.div>
      </div>
    </>
  );
};

export default ModalProduct;
