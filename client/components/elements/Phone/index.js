import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Phone() {
  return (
    <div className="phone__wrapper">
      <FontAwesomeIcon className="phone__icon" icon={["fas", "phone-alt"]} />
      1900 1570
    </div>
  );
}
