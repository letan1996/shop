import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default function Contact() {
  return (
    <ul className="contact nav">
      <li>
        <FontAwesomeIcon
          className="icon-map"
          icon={["fas", "map-marker-alt"]}
          size="1x"
        />{" "}
        Thanh Quýt 3, Điện Thắng Trung, Điện Bàn, Quảng Nam
      </li>
      <li>
        <FontAwesomeIcon
          className="icon-mail"
          icon={["fas", "envelope"]}
          size="1x"
        />{" "}
        letutan500@gmail.com
      </li>
    </ul>
  );
}
