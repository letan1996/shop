import React from 'react'
import { useTrans } from 'hooks';
import Link from 'next/link';

export default function Language() {
  const trans = useTrans();
  const lang = trans?.lang ?? '';
  return (
    <div className="lang">
      <span className="lang-current">{lang}</span>
      <ul className="lang-content">
        <li className="lang-item">
          <Link href="/" locale="vi">
            <a>VietNamese</a>
          </Link>
        </li>
        <li className="lang-item">
          <Link href="/" locale="en">
            <a>English</a>
          </Link>
        </li>
      </ul>
    </div>
  )
}
