import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { useSelector } from "react-redux";

export default function Cart() {
  const { listProduct } = useSelector((state) => state.cart);
  return (
    <div className="cart__item">
      <FontAwesomeIcon className="cart-icon" icon={["fas", "cart-plus"]} />
      {listProduct.length !== 0 && (
        <div className="cart__count">{listProduct.length}</div>
      )}
    </div>
  );
}
