import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function LoadingIcon({ size }) {
  return <FontAwesomeIcon icon={["fas", "spinner"]} spin size={size} />;
}
