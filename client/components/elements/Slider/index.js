import React, { useEffect, useState } from "react";
import {
  useTransition,
  animated,
  useSpringRef,
  config,
} from "@react-spring/web";
import { useInterval } from "hooks";
const slides = [
  {
    url: "https://tools.dalathasfarm.com/assets/2021/2021-07/1ad5466fd247e063a9ff635346f5d42a.jpg",
    title: "Lorem ipsum dolor sit amet",
    text: "Consectetur adipiscing elit. Cras euismod scelerisque nisl id viverra.",
  },
  {
    url: "https://tools.dalathasfarm.com/assets/2021/2021-07/14f9dd4e326294e9612e3eb96b5fb099.jpg",
    title: "Mauris eget tincidunt metus",
    text: "Nec tempor mi. Aenean euismod nunc ligula, ut tincidunt elit pretium at. Integer a molestie purus. Aliquam erat volutpat. ",
  },
  {
    url: "https://tools.dalathasfarm.com/assets/2021/2021-05/3d257a15f760019fb19697bf94659ae3.jpg",
    title: "Nullam viverra est",
    text: "Vitae pulvinar tincidunt. Donec vel magna nunc. Cras pharetra cursus lectus, a pharetra orci iaculis non.",
  },
  {
    url: "https://tools.dalathasfarm.com/assets/2021/2021-04/e3f998cdf21fe1b4f70f7d7b23aeadb3.jpg",
    title: "Sed accumsan",
    text: "Condimentum leo, vel fermentum massa pulvinar vitae. Nam a felis libero. Fusce pellentesque dignissim finibus.",
  },
  {
    url: "https://tools.dalathasfarm.com/assets/2021/2021-05/3d257a15f760019fb19697bf94659ae3.jpg",
    title: "Praesent varius",
    text: "Massa vitae ornare vestibulum, ligula elit ultrices mi, nec scelerisque odio nulla ut eros.",
  },
];
const Slide = ({ style, background }) => {
  return (
    <animated.div
      style={{
        position: "absolute",
        height: "100%",
        width: "100%",
        backgroundColor: "gray",
        willChange: "opacity",
        ...style,
      }}
    >
      <img className="slide__img" src={background}></img>
    </animated.div>
  );
};

export default function Slider() {
  const [[index, dir], setIndex] = useState([0, 0]);
  const transRef = useSpringRef();
  const [moving, setMoving] = useState(true);
  const transitionsSlide = useTransition(index, {
    ref: transRef,
    from: {
      transform: `translate3d(${dir === 1 ? 100 : -100}%,0,0)`,
    },
    enter: {
      transform: "translate3d(0%,0,0)",
    },
    leave: {
      transform: `translate3d(${dir === 1 ? -100 : 100}%,0,0)`,
    },
    config: config.slow,
  });

  useInterval(
    () => {
      slideRight();
    },
    moving ? 3000 : null
  );

  useEffect(() => {
    transRef.start();
  }, [index]);

  const slideLeft = () => {
    setIndex([(index - 1 + slides.length) % slides.length, -1]);
  };

  const slideRight = () => {
    setIndex([(index + 1) % slides.length, 1]);
  };

  const onNext = () => {
    setMoving(false);
    setTimeout(() => {
      setMoving(true);
    }, 2000);
    slideRight();
  };

  const onPrev = () => {
    setMoving(false);
    setTimeout(() => {
      setMoving(true);
    }, 2000);
    slideLeft();
  };

  return (
    <div className="slide__container">
      {transitionsSlide((style, i) => {
        const item = slides[i];
        return (
          <Slide
            key={i}
            style={{
              ...style,
            }}
            background={item.url}
          />
        );
      })}
      <div className="prev" onClick={onPrev}>
        &#10094;
      </div>
      <div className="next" onClick={onNext}>
        &#10095;
      </div>
    </div>
  );
}
