import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "redux/actions";
import { addCart, deleteCart } from "redux/actions/cart";
import { renderCurrencyVN } from "utils/common";
import Fab from "../Fab";
const placeholderImg = "/product-img-placeholder.svg";

export default function ProductCard({ product }) {
  const [toggle, setToggle] = useState(false);
  const [loadingCart, setLoadingCart] = useState(false);
  const { listProduct } = useSelector((state) => state.cart);
  const isHasCart = listProduct.some((p) => p.slug === product.slug);
  const dispatch = useDispatch();

  const openModalProduct = (data) => {
    dispatch(openModal(data));
  };

  const handleOnMouseOver = () => {
    setToggle(true);
  };

  const handleOnMouseOut = () => {
    setToggle(false);
  };

  const handleAddCart = (item) => {
    setLoadingCart(true);
    setTimeout(() => {
      setLoadingCart(false);
      dispatch(addCart(item));
    }, 400);
  };

  const handleRemoveCart = (item) => {
    setLoadingCart(true);
    setTimeout(() => {
      setLoadingCart(false);
      dispatch(deleteCart(item));
    }, 400);
  };

  return (
    <Link href={`/product/${product.slug}`}>
      <div
        className="product__item"
        onMouseOver={handleOnMouseOver}
        onMouseOut={handleOnMouseOut}
      >
        {product?.images && (
          <div className="product__bgWrap">
            <Image
              alt="Mountains"
              src={product.images[0]?.url || placeholderImg}
              layout="fill"
              objectFit="cover"
            />
          </div>
        )}
        <div className="product__body">
          <span className="product__text-name">{product.title}</span>
          <span className="product__price-current">
            {renderCurrencyVN(product.priceCurrent)}
          </span>
          <span className="product__price-old">
            {renderCurrencyVN(product.priceOld)}
          </span>
        </div>
        <Fab
          toggle={toggle}
          listButton={[
            {
              id: 1,
              icon: isHasCart ? ["fas", "check-circle"] : ["fas", "cart-plus"],
              handleClick: () =>
                !isHasCart ? handleAddCart(product) : handleRemoveCart(product),
              color: isHasCart ? "green" : "#000",
              isLoading: loadingCart,
              isHasCart,
            },
            // {
            //   id: 4,
            //   icon: ["fas", "eye"],
            //   handleClick: () => openModalProduct(product),
            // },
          ]}
        />
      </div>
    </Link>
  );
}
