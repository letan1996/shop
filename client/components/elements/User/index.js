import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function User() {
  return (
    <div className="user__info">
      <FontAwesomeIcon className="user-icon" icon={["fas", "user"]} />
      <div className="user-content">
        <div className="user__title">Tài khoản</div>
        <div className="user__name">
          Lee Tan{" "}
          <FontAwesomeIcon
            className="dropdown-icon"
            icon={["fas", "chevron-down"]}
          />
        </div>
      </div>
      <div className="user-dropdown__content">
        <div className="user-dropdown__item">Thông tin chung</div>
        <div className="user-dropdown__item">Đăng xuất</div>
      </div>
    </div>
  );
}
