import React, { useRef } from "react";
import { animated, useSprings } from "react-spring";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import LoadingIcon from "../LoadingIcon";

const ButtonElement = ({ style, item }) => {
  return (
    <a
      onClick={(e) => {
        e.preventDefault();
        item.handleClick();
      }}
    >
      <animated.div
        style={{
          position: "absolute",
          height: "46px",
          width: "46px",
          display: "flex",
          right: "10px",
          bottom: "20%",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: "50%",
          boxShadow: "2px 2px 20px #00000017",
          backgroundColor: "white",
          willChange: "opacity",
          ...style,
        }}
      >
        {item.isLoading ? (
          <LoadingIcon size="lg" />
        ) : (
          <FontAwesomeIcon icon={item.icon} color={item.color} size="lg" />
        )}
      </animated.div>
    </a>
  );
};
export default function Fab(props) {
  const { listButton, toggle } = props;

  const ref = useRef();
  const springs = useSprings(
    listButton.length,
    listButton.map((item, index) => {
      return {
        to: {
          opacity: 1,
          transform: `translate3d(0,${-index * 50}px,0)`,
        },
      };
      // if (item.isHasCart) {
      //   return {
      //     to: {
      //       opacity: 1,
      //       transform: `translate3d(0,${-index * 50}px,0)`,
      //     },
      //   };
      // } else
      //   return {
      //     reverse: toggle ? false : true,
      //     immediate: !ref.current,
      //     from: {
      //       opacity: 0,
      //       transform: `translate3d(0,0px,0)`,
      //     },
      //     to: {
      //       opacity: 1,
      //       transform: `translate3d(0,${-index * 50}px,0)`,
      //     },
      //   };
    })
  ); // Create springs, each corresponds to an item, controlling its transform, scale, etc.

  return (
    <div ref={ref}>
      {springs.map((animation, i) => {
        const elm = listButton[i];
        return <ButtonElement key={i} style={animation} item={elm} />;
      })}
    </div>
  );
}
