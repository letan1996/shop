import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Search() {
  return (
    <div className="search__wrapper">
      <input className="search__input" placeholder="Nhập từ khoá tìm kiếm" />
      <div className="search__btn">
        <FontAwesomeIcon
          className="icon-search"
          icon={["fas", "search"]}
          size="1x"
        />
      </div>
      <div className="history__wrapper">
        <ul className="history__list">
          <li className="history__item">1121212121</li>
          <li className="history__item">221212121</li>
          <li className="history__item">3212121</li>
          <li className="history__item">4212121</li>
          <li className="history__item">1121212121</li>
          <li className="history__item">221212121</li>
          <li className="history__item">3212121</li>
          <li className="history__item">4212121</li>
          <li className="history__item">1121212121</li>
          <li className="history__item">221212121</li>
          <li className="history__item">3212121</li>
          <li className="history__item">4212121</li>
        </ul>
      </div>
    </div>
  );
}
