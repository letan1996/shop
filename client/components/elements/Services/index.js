import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Services() {
  return (
    <div className="service">
      <div className="grid wide">
        <div className="row">
          <div className="c-3">
            <div className="service__item">
              <div className="service__icon">
                <FontAwesomeIcon icon={["fas", "shipping-fast"]} />
              </div>
              <div className="service__name">Giao hàng</div>
              <div className="service__desc">Miễn phí giao hàng</div>
            </div>
          </div>
          <div className="c-3">
            <div className="service__item">
              <div className="service__icon">
                <FontAwesomeIcon icon={["fas", "shipping-fast"]} />
              </div>
              <div className="service__name">Giao hàng</div>
              <div className="service__desc">Miễn phí giao hàng</div>
            </div>
          </div>
          <div className="c-3">
            <div className="service__item">
              <div className="service__icon">
                <FontAwesomeIcon icon={["fas", "shipping-fast"]} />
              </div>
              <div className="service__name">Giao hàng</div>
              <div className="service__desc">Miễn phí giao hàng</div>
            </div>
          </div>
          <div className="c-3">
            <div className="service__item">
              <div className="service__icon">
                <FontAwesomeIcon icon={["fas", "shipping-fast"]} />
              </div>
              <div className="service__name">Giao hàng</div>
              <div className="service__desc">Miễn phí giao hàng</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
