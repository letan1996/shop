import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function SideBar() {
  const [isShow, setIsShow] = useState(true);

  const toggle = () => {
    setIsShow((prevState) => !prevState);
  };

  return (
    <div className="sidebar">
      <div className="sidebar__label" onClick={toggle}>
        Danh mục{" "}
        <FontAwesomeIcon className="menu__icon" icon={["fas", "bars"]} />
      </div>
      <ul className={`menu ${!isShow ? "hidden" : ""}`}>
        <li className="menu__item flex-row">
          Hoa chậu thiết kế{" "}
          <FontAwesomeIcon
            className="sidebar-icon"
            icon={["fas", "chevron-right"]}
          />
          <div className="menu-sub">
            <ul>
              <li>1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
            </ul>
          </div>
        </li>
        <li className="menu__item flex-row">
          Hoa xinh giá tốt{" "}
          <FontAwesomeIcon
            className="sidebar-icon"
            icon={["fas", "chevron-right"]}
          />
          <div className="menu-sub">
            <ul>
              <li>5</li>
              <li>6</li>
              <li>7</li>
              <li>8</li>
            </ul>
          </div>
        </li>
        <li className="menu__item flex-row">Lan hồ điệp</li>
        <li className="menu__item flex-row">Hoa chậu</li>
        <li className="menu__item flex-row">Hoa cắt cành</li>
        <li className="menu__item flex-row">Phụ liệu hoa</li>
      </ul>
    </div>
  );
}
