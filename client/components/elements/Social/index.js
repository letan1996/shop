import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Social() {
  return (
    <ul className="social nav">
      <li className="social__item social-twitter">
        <FontAwesomeIcon className="icon-twitter" icon={["fab", "twitter"]} />
      </li>
      <li className="social__item social-facebook">
        <FontAwesomeIcon className="icon-facebook" icon={["fab", "facebook"]} />
      </li>
    </ul>
  );
}
