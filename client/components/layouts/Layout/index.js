import React from "react";
import { Footer, Header } from "components";
export default function Layout({ children }) {
  return (
    <div className="layout__wrapper">
      <Header />
      {children}
      <Footer />
    </div>
  );
}
