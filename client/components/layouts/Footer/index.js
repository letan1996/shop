import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Footer() {
  return (
    <div className="footer">
      <div className="grid wide">
        <div className="row">
          <div className="c-6">
            <div className="row">
              <div className="c-6">
                <strong className="logo__text">Woodmart</strong>
                <p className="desc">
                  Condimentum adipiscing vel neque dis nam parturient orci at
                  scelerisque neque dis nam parturient.
                </p>
                <ul className="footer__contact">
                  <li className="footer__contact-item">
                    <FontAwesomeIcon
                      className="footer__icon"
                      icon={["fas", "location-arrow"]}
                    />
                    Điện Thắng Trung, huyện Điện Bàn, tỉnh Quảng Nam
                  </li>
                  <li className="footer__contact-item">
                    <FontAwesomeIcon
                      className="footer__icon"
                      icon={["fas", "mobile"]}
                    />
                    0702486903
                  </li>
                  <li className="footer__contact-item">
                    <FontAwesomeIcon
                      className="footer__icon"
                      icon={["fas", "envelope"]}
                    />
                    letutan500@gmail.com
                  </li>
                </ul>
              </div>
              <div className="c-6">
                <ul className="footer__post">
                  <li className="footer__post-item">
                    <div className="footer__post-image">
                      <img src="https://z9d7c4u6.rocketcdn.me/wp-content/uploads/2016/07/blog-12-75x65.jpg" />
                    </div>
                    <div className="footer__post-desc">
                      <div className="footer__post-title">
                        A companion for extra sleeping
                      </div>
                      <div className="footer__post-time">
                        July 23, 2016 1 Comment
                      </div>
                    </div>
                  </li>
                  <li className="footer__post-item">
                    <div className="footer__post-image">
                      <img src="https://z9d7c4u6.rocketcdn.me/wp-content/uploads/2016/07/blog-12-75x65.jpg" />
                    </div>
                    <div className="footer__post-desc">
                      <div className="footer__post-title">
                        A companion for extra sleeping
                      </div>
                      <div className="footer__post-time">
                        July 23, 2016 1 Comment
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="c-6">
            <div className="row">
              <div className="c-4"></div>
              <div className="c-4">bBb</div>
              <div className="c-4">aAA</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
