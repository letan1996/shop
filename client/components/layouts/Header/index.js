import Head from "next/head";
import React from "react";
import {
  Cart,
  Contact,
  Logo,
  MainMenu,
  Phone,
  Search,
  SideBar,
  Social,
  User,
} from "components";

export default function Header() {
  return (
    <div className="header">
      <Head>
        <title>My page title</title>
        <meta property="og:title" content="My page title" key="title" />
      </Head>
      <Head>
        <style></style>
        <meta property="og:title" content="My new title" key="title" />
      </Head>
      <div className="header__top">
        <div className="grid wide">
          <div className="flex-row">
            <div className="flex-col flex-left">
              <Contact />
            </div>
            <div className="flex-col flex-right">
              <Social />
            </div>
          </div>
        </div>
      </div>
      <div className="header__main">
        <div className="grid wide">
          <div className="flex-row">
            <div className="flex-col flex-left">
              <Logo />
            </div>
            <div className="flex-col flex-grow">
              <Search />
            </div>
            <div className="flex-col flex-right">
              <ul className="flex-row nav">
                <li className="flex-col">
                  <Phone />
                </li>
                <li className="flex-col">
                  <User />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="header__bottom">
        <div className="grid wide">
          <div className="flex-row">
            <div className="flex-col flex-left sidebar__wrapper">
              <SideBar />
            </div>
            <div className="flex-col flex-grow nav__wrapper">
              <MainMenu />
            </div>
            <div className="flex-col flex-right">
              <Cart />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
