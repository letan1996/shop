import { combineReducers } from "redux";
import modal from "./modal";
import cart from "./cart";
export default combineReducers({
  modal,
  cart,
});
