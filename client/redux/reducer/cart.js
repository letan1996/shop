import { actionTypes } from "redux/actionTypes";

const initialState = {
  listProduct: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_ITEM_CART:
      const item = state.listProduct.find(
        (item) => item.slug === action.data.slug
      );
      (item) => item.slug === action.data.slug;
      if (!item) {
        return {
          ...state,
          listProduct: [{ ...action.data, soLuong: 1 }, ...state.listProduct],
        };
      } else {
        return {
          ...state,
          listProduct: state.listProduct.map((item) => {
            if (item.slug === action.data.slug) {
              return { ...item, soLuong: item.soLuong + 1 };
            } else {
              return item;
            }
          }),
        };
      }
    case actionTypes.DELETE_ITEM_CART:
      return {
        ...state,
        listProduct: state.listProduct.filter(
          (item) => item.slug !== action.data.slug
        ),
      };
    default:
      return state;
  }
}

export default reducer;
