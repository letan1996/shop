import { actionTypes } from "redux/actionTypes";

const initialState = {
  isVisibleModal: false,
  modalProps: null,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.TOGGLE_MODAL: {
      return { ...state, isVisibleModal: !state.isVisibleModal };
    }

    case actionTypes.OPEN_MODAL:
      return {
        ...state,
        isVisibleModal: true,
        modalProps: action.data,
      };

    case actionTypes.CLOSE_MODAL:
      return {
        ...state,
        isVisibleModal: false,
      };

    default:
      return state;
  }
}

export default reducer;
