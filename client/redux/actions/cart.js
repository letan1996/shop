import { actionTypes } from "redux/actionTypes";
export function addCart(data) {
  return {
    type: actionTypes.ADD_ITEM_CART,
    data,
  };
}
export function deleteCart(data) {
  return {
    type: actionTypes.DELETE_ITEM_CART,
    data,
  };
}
export function increase(data) {
  return {
    type: actionTypes.INCREASE_ITEM_CART,
    data,
  };
}
export function decrease(data) {
  return {
    type: actionTypes.DECREASE_ITEM_CART,
    data,
  };
}
