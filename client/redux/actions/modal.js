import { actionTypes } from "redux/actionTypes";
export function openModal(data) {
  return {
    type: actionTypes.OPEN_MODAL,
    data,
  };
}
export function closeModal() {
  return {
    type: actionTypes.CLOSE_MODAL,
  };
}
export function toggleModal() {
  return {
    type: actionTypes.TOGGLE_MODAL,
  };
}
