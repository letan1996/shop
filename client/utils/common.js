export const renderCurrencyVN = (number) => {
  return number.toLocaleString("it-IT", { currency: "VND" });
};
