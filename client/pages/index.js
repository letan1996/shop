import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Layout,
  ModalProduct,
  ProductCard,
  Services,
  Slider,
} from "components";
import { useSelector } from "react-redux";
import { useTransition } from "react-spring";

export default function Home() {
  const { isVisibleModal } = useSelector((state) => state.modal);

  const transitions = useTransition(isVisibleModal, {
    from: { opacity: 0, transform: "translateY(-40px)" },
    enter: { opacity: 1, transform: "translateY(0px)" },
    leave: { opacity: 0, transform: "translateY(-40px)" },
  });

  return (
    <Layout>
      <div className="grid wide">
        {/* Slide */}
        <div className="flex-row slide-wrapper">
          <Slider />
        </div>
        {/* Services */}
        <div className="flex-row">
          <Services />
        </div>
        {/* Sản phẩm bán chạy */}
        <div className="flex-row category">
          <div className="flex-left category-heading">
            <div className="flex-row">
              <div className="flex-left category-title">
                <img src="/tree.png" />
                <h5>Sản phẩm bán chạy</h5>
              </div>
              <div className="flex-right">
                <div className="more">
                  Xem thêm{" "}
                  <FontAwesomeIcon
                    className="more-icon"
                    icon={["fas", "chevron-right"]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "1",
                title: "Hoa hồng phớt",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/p1.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "2",
                title: "Hoa hồng vàng",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/p2.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "3",
                title: "Hoa hồng vàng",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/p3.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "4",
                title: "Hoa hồng đỏ",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/p4.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "5",
                title: "Hoa hồng xanh",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/p5.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "7",
                title: "Hoa hồng cam",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product3.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-4 lg-3">
            <ProductCard
              product={{
                slug: "8",
                title: "Rau củ, quả",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product4.jpeg",
                  },
                ],
              }}
            />
          </div>
        </div>
        {/* Sản phẩm nổi bật */}
        <div className="flex-row category">
          <div className="flex-left category-heading">
            <div className="flex-row">
              <div className="flex-left category-title">
                <img src="/tree.png" />
                <h5>Sản phẩm nổi bật</h5>
              </div>
              <div className="flex-right">
                <div className="more">
                  Xem thêm{" "}
                  <FontAwesomeIcon
                    className="more-icon"
                    icon={["fas", "chevron-right"]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "9",
                title: "Cây Đa Búp Đỏ",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product1.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "10",
                title: "Cây Bàng Singapore",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product2.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "11",
                title: "Cây Trầu Bà Đế Vương Đỏ",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product3.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "12",
                title: "Cây Ngũ Gia Bì",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product4.jpeg",
                  },
                ],
              }}
            />
          </div>

          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "13",
                title: "Cây Thiết Mộc Lan",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product1.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "14",
                title: "Cây Thường Xuân",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product2.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "15",
                title: "Cây Cẩm Nhung",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product3.jpeg",
                  },
                ],
              }}
            />
          </div>
          <div className="c-12 sm-6 md-3 lg-3">
            <ProductCard
              product={{
                slug: "16",
                title: "Cây Phát Lộc Thủy Sinh",
                priceOld: 330000,
                priceCurrent: 200000,
                images: [
                  {
                    id: 1,
                    url: "/product4.jpeg",
                  },
                ],
              }}
            />
          </div>
        </div>
      </div>
      {/* Modal Sản phẩm */}
      {transitions((style, item) => item && <ModalProduct style={style} />)}
    </Layout>
  );
}
